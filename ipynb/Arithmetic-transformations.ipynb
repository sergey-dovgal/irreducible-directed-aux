{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "1ce18e53",
   "metadata": {},
   "source": [
    "# Coefficient generating functions"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "05e0468d",
   "metadata": {},
   "source": [
    "We are using a Coefficient GF in the form\n",
    "\\\\[\n",
    "    F^\\circ(z, w) = \\sum_{m} \\sum_\\ell f_{m,\\ell}^\\circ\n",
    "    \\alpha^{-\\frac{1}{\\beta} \\binom{m}{2}} z^m w^\\ell\n",
    "\\\\]\n",
    "for the coefficients \\\\( (f_{m,\\ell}^\\circ)_{m \\geqslant M, \\ell \\geqslant 0} \\\\) of the complete asymptotic expansion\n",
    "\\\\[\n",
    "    f_n \\approx \\alpha^{\\beta \\binom{n}{2}} \\left[\n",
    "        \\sum_m \\sum_\\ell  \\alpha^{-mn} n^{\\underline{\\ell}}\n",
    "        f_{m,\\ell}^\\circ\n",
    "    \\right] \\, .\n",
    "\\\\]\n",
    "(Note that the summation boundaries are omitted for simplicity)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a332012f",
   "metadata": {},
   "source": [
    "We are using a dedicated operator \\\\( \\mathcal Q_\\alpha^\\beta \\\\) to convert a formal power series\n",
    "\\\\[\n",
    "    F(x) = \\sum_n f_n \\dfrac{x^n}{n!}\n",
    "\\\\]\n",
    "into its Coefficient GF \\\\( F^\\circ(z, w) \\\\), taking into account the growth parameters:\n",
    "\\\\[\n",
    "    [\\mathcal Q_\\alpha^\\beta F](z, w) = F^\\circ(z, w) \\, .\n",
    "\\\\]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ed467178",
   "metadata": {},
   "source": [
    "It is reasonable to check the arithmetics behind the two propositions providing the multiplication and composition operations for these series. According to Bender's theorem, it is sufficient to treat only one convolution from multiplication. We are providing a version with only one non-zero term for simplicity."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0b90d8e8",
   "metadata": {},
   "source": [
    "## Proposition\n",
    "Suppose that \\\\( \\mathcal Q_\\alpha^\\beta B(x) = 0 \\\\). Then,\n",
    "\\\\[\n",
    "    \\boxed{\n",
    "    (\\mathcal Q_\\alpha^\\beta(A \\cdot B))(z,w) =\n",
    "    B(\\alpha^{\\frac{\\beta + 1}{2}} z^\\beta w)\n",
    "    (\\mathcal Q_\\alpha^\\beta A)(z, w)\n",
    "    }\n",
    "\\\\]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6d035106",
   "metadata": {},
   "source": [
    "Without going into technical details about the growth rate of the coefficients, we are only going to check the formal part of this proposition.\n",
    "\n",
    "**Part I.** If\n",
    "\\\\[\n",
    "    \\left(\n",
    "        \\sum_{m,\\ell} c_{m,\\ell}^\\circ \\alpha^{- \\frac{1}{\\beta} \\binom{m}{2}}\n",
    "        z^m w^\\ell\n",
    "    \\right) = \n",
    "    \\left(\n",
    "        \\sum_k\n",
    "        \\left(\n",
    "            \\alpha^{\\frac{\\beta+1}{2}}\n",
    "            z^\\beta w\n",
    "        \\right)^k\n",
    "        b_k \\dfrac{z^k}{k!}\n",
    "    \\right)\n",
    "    \\left(\n",
    "        \\sum_{m,\\ell} a_{m,\\ell}^\\circ \\alpha^{- \\frac{1}{\\beta} \\binom{m}{2}}\n",
    "        z^m w^\\ell\n",
    "    \\right) \\, ,\n",
    "\\\\]\n",
    "then\n",
    "\\\\[\n",
    "    c_{m,\\ell}^\\circ = \\sum_k\n",
    "    \\dfrac{b_k}{k!} a_{m-\\beta k, \\ell-k}^\\circ\n",
    "    \\dfrac{\\alpha^{km}}{\\alpha^{\\beta \\binom{k}{2}}}\n",
    "\\\\]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b0fdac81",
   "metadata": {},
   "source": [
    "**Part II.**\n",
    "\\\\[\n",
    "    \\sum_k \\binom{n}{k} b_k a_{n-k}\n",
    "    \\approx\n",
    "    \\alpha^{\\beta \\binom{n}{2}}\n",
    "    \\sum_m \\sum_\\ell \\alpha^{-mn} n^{\\underline{\\ell}}\n",
    "    c_{m,\\ell}^\\circ \\, ,\n",
    "\\\\]\n",
    "where \\\\( c_{m,\\ell}^\\circ \\\\) is defined in Part I."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aafb50ee",
   "metadata": {},
   "source": [
    "## Preparation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "7ee8d16c",
   "metadata": {},
   "outputs": [],
   "source": [
    "import sympy as sp"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "3b696ac0",
   "metadata": {},
   "outputs": [],
   "source": [
    "n,m,k,ell = sp.symbols('n,m,k,ell')\n",
    "beta = sp.Symbol('beta')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "26607a75",
   "metadata": {},
   "outputs": [],
   "source": [
    "def choose_2(n):\n",
    "    \"\"\"Symbolic \\binom{n}{2}\n",
    "    \"\"\"\n",
    "    return n * (n-1) / 2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "42b3c1b1",
   "metadata": {},
   "source": [
    "## Part I, calculation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aabb36ec",
   "metadata": {},
   "source": [
    "Formally, we have"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4b92ffdc",
   "metadata": {},
   "source": [
    "\\\\[\n",
    "    c_{m,\\ell}^\\circ = \\sum_{k}\n",
    "    \\dfrac{b_k}{k!} a_{m-\\beta k, \\ell-k}^\\circ\n",
    "    \\alpha^{\\frac{(\\beta+1)k}{2} + \\frac{1}{\\beta}\\binom{m}{2} - \\frac{1}{\\beta}\\binom{m-\\beta k}{2}}\n",
    "\\\\]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0509c254",
   "metadata": {},
   "source": [
    "**Assertion 1.**\n",
    "\\\\[\n",
    "\\boxed{\n",
    "    \\alpha^{\\frac{(\\beta+1)k}{2} + \\frac{1}{\\beta}\\binom{m}{2} - \\frac{1}{\\beta}\\binom{m-\\beta k}{2}} =\n",
    "    \\dfrac{\\alpha^{km}}{\\alpha^{\\beta \\binom{k}{2}}}\n",
    "}\n",
    "\\\\]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "c3ebac65",
   "metadata": {},
   "outputs": [],
   "source": [
    "LHS = (beta + 1) * k / 2 + choose_2(m) / beta - choose_2(m - beta*k) / beta\n",
    "RHS = k * m - beta * choose_2(k)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "7f09fd65",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Assertion 1 verified\n"
     ]
    }
   ],
   "source": [
    "assert (LHS - RHS).simplify() == 0\n",
    "print(\"Assertion 1 verified\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fecaa8e7",
   "metadata": {},
   "source": [
    "## Part II, calculation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d9108d65",
   "metadata": {},
   "source": [
    "**Assertion 2.**\n",
    "\\\\[\n",
    "    \\boxed{n^{\\underline{k}} (n-k)^{\\underline{\\ell}} = n^{\\underline{k+\\ell}}}\n",
    "\\\\]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0de577f1",
   "metadata": {},
   "source": [
    "Let us define a falling factorial by using sympy's built-in binomial.\n",
    "Upon simplifying, it will be expressed in terms of the Gamma function, which is not very convenient.\n",
    "However, this allows us to compare the two expressions and conclude they are equal."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "e2470ec8",
   "metadata": {},
   "outputs": [],
   "source": [
    "def ff(n,k):\n",
    "    \"\"\"Falling factorial\n",
    "    \"\"\"\n",
    "    return sp.binomial(n,k) * sp.factorial(k)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "27a3b638",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Assertion 2 verified\n"
     ]
    }
   ],
   "source": [
    "assert (ff(n-k,ell) * ff(n,k) - ff(n, k+ell)).simplify() == 0\n",
    "print(\"Assertion 2 verified\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "81eeb212",
   "metadata": {},
   "source": [
    "Note that by shifting the sequence \\\\( a_n \\mapsto a_{n-k} \\\\), we obtain, using the asymptotic expansion\n",
    "\\\\[\n",
    "    \\binom{n}{k} a_{n-k} \\approx\n",
    "    \\dfrac{1}{k!} \\alpha^{\\beta \\binom{n-k}{2}}\n",
    "    \\sum_m \\sum_\\ell \\alpha^{-m(n-k)}\n",
    "    n^{\\underline{k}} (n-k)^{\\underline{\\ell}} a_{m,\\ell}^\\circ\n",
    "\\\\]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6e3f70b7",
   "metadata": {},
   "source": [
    "As we mentioned before, there is a simplification\n",
    "\\\\( n^{\\underline{k}} (n-k)^{\\underline{\\ell}} = n^{\\underline{k+\\ell}} \\\\).\n",
    "\n",
    "In the course of the proof, we shift the indices\n",
    "\\\\[\n",
    "    \\begin{array}{rcl}\n",
    "    m    &\\mapsto& m-\\beta k \\\\\n",
    "    \\ell &\\mapsto& \\ell - k\n",
    "    \\end{array}\n",
    "\\\\]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d3ef972c",
   "metadata": {},
   "source": [
    "**Assertion 3.**\n",
    "\\\\[\n",
    "\\boxed{\n",
    "\\left.\\alpha^{\\beta \\binom{n-k}{2}}\n",
    "\\alpha^{-m(n-k)}\n",
    "n^{\\underline{k+\\ell}} \\right|_{m \\mapsto m-\\beta k, \\ell \\mapsto \\ell-k} =\n",
    "\\alpha^{\\beta \\binom{n}{2}}\n",
    "\\alpha^{-mn} n^{\\underline{\\ell}}\n",
    "\\dfrac{\\alpha^{km}}{\\alpha^{\\beta \\binom{k}{2}}}\n",
    "}\n",
    "\\\\]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "03c79b0e",
   "metadata": {},
   "source": [
    "For simplicity of assisted verification, let us omit the part\n",
    "\\\\( n^{\\underline{k+\\ell}}|_{\\ell \\mapsto \\ell-k} = n^{\\underline{\\ell}} \\\\)\n",
    "as obvious."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "69404935",
   "metadata": {},
   "outputs": [],
   "source": [
    "LHS = beta * choose_2(n-k) - m*(n-k)\n",
    "RHS = beta * choose_2(n) - m*n + k*m - beta * choose_2(k)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "dac33073",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Assertion 3 verified.\n"
     ]
    }
   ],
   "source": [
    "assert (LHS.subs({m: m - beta*k, ell: ell - k}) - RHS).simplify() == 0\n",
    "print(\"Assertion 3 verified.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5e4b4792",
   "metadata": {},
   "source": [
    "Combining the three assertions, we get the proof of the proposition."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
