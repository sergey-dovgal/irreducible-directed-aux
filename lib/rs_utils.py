import sympy as sp
from sympy.polys import ring, QQ, RR
from sympy.polys.ring_series import rs_mul
from sympy.polys.ring_series import rs_exp, rs_pow
from sympy.polys.ring_series import rs_series_inversion
from sympy.polys.ring_series import rs_diff, rs_fun
from sympy.polys.ring_series import rs_trunc
import sympy.polys.ring_series as ring_series


def tuple_replace(t, pos, value):
    """
    supplementary function to replace an element in the tuple is required
    because the powers of formal variables are represented by tuples
    """
    l = list(t)
    l[pos] = value
    return tuple(l)


def mcoeff_at(poly, x, idx):
    """
    Operation of coefficient extraction at `x^n`.
    If the input is of the form `F(z, w) = \\sum_n f_n(w) z^n`,
    the function returns `f_n(w)`, where `w` can contain several variables
    """
    get = poly.get
    R = poly.ring
    result = R.zero
    iv = R.gens.index(x)
    for k in poly.as_expr_dict().keys():
        if k[iv] == idx:
            r_idx = tuple_replace(k, iv, 0)
            if r_idx not in result:
                result[r_idx] = poly[k]
            else:
                result[r_idx] += poly[k]
    return result


def rs_fourier(p, z, w, N, M, inverse=False):
    """
    Coefficient-wise multiplication of the coefficient at `z^n` by `exp(-w n^2/2)`
    """
    R = p.ring
    result = R.zero
    for n in range(N):
        if inverse:
            result += mcoeff_at(p, z, n) * rs_exp(w * n**2/2, w, M) * z**n
        else:
            result += mcoeff_at(p, z, n) * rs_exp(-w * n**2/2, w, M) * z**n
    return result


def rs_fourier_simple(p, z, w, N, M, inverse=False):
    """
    Coefficient-wise multiplication of the coefficient at `z^n` by `exp(-w n^2/2)`
    """
    R = p.ring
    result = R.zero
    for n in range(N):
        if inverse:
            result += mcoeff_at(p, z, n) * rs_pow(1+w, n*(n-1)//2, w, M) * z**n
        else:
            result += mcoeff_at(p, z, n) * rs_pow(1+w, -n*(n-1)//2, w, M) * z**n
    return result


def rs_generalised_exp(r, z, w, N, M):
    """
    Implementation of the generalised deformed exponent from the paper.
    Can be defined as `rs_fourier[e^{-z} * (1 - zw)**r]`
    Returns a bivariate polynomial, the degrees with respect to `z` and `w`
    are truncated to `N-1` and `M-1`
    """
    basic_poly = rs_mul(rs_exp(-z, z, N), rs_pow(1 - z*w, r, z, N), z, N)
    return rs_trunc(rs_fourier(basic_poly, z, w, N, M), w, M)
