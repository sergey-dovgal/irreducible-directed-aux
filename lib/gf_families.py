import sympy as sp
from sympy.polys import ring, QQ, RR
from sympy.polys.ring_series import rs_mul, rs_subs
from sympy.polys.ring_series import rs_exp, rs_pow, rs_log
from sympy.polys.ring_series import rs_series_inversion
from sympy.polys.ring_series import rs_trunc

from rs_utils import mcoeff_at, rs_generalised_exp
from rs_utils import rs_fourier, rs_fourier_simple

from sympy import binomial

###############################################
##### GFs OF GRAPH FAMILIES
###############################################

def gf_graphs(z, prec):
    """Exponential GF of simple graphs

    Args:
        z (sympy.polys.rings.PolyElement): first generator of the ring;
            the variable marking the number of nodes
        prec (int): series precision with respect to `z`

    Returns:
        sympy.polys.rings.PolyElement

    """
    return sum([
        z**n / sp.factorial(n)
        * sp.Rational(2,1) ** (n * (n-1) // 2)
        for n in range(prec)
    ])


def gf_irreducible_tournaments(z, prec):
    """Exponential GF of irreducible tournaments

    Args:
        z (sympy.polys.rings.PolyElement): first generator of the ring;
            the variable marking the number of nodes
        prec (int): series precision with respect to `z`

    Returns:
        sympy.polys.rings.PolyElement

    """
    return 1 - rs_series_inversion(gf_graphs(z, prec), z, prec)

#######################################
##### ROBINSON'S DELTA OPERATOR
#######################################

def Delta(gf, z, alpha, beta, prec):
    """Operator `Delta_alpha^beta`.

    Args:
        z (sympy.polys.rings.PolyElement): the corresponding
            generator of the ring.
        alpha (sp.Rational): exponent base
        beta (sp.Rational): exponent power
        prec (int): series precision with respect to `z`

    Returns:
        sympy.polys.rings.PolyElement

    """
    R = gf.ring
    result = R.zero
    for n in range(prec):
        result += (
            mcoeff_at(gf, z, n)
            / alpha ** (beta * binomial(n,2))
            * z**n
        )
    return result


########################################
##### GF OF DIGRAPH FAMILIES
########################################

def gf_strongly_connected(z, N):
    """Exponential GF of strongly connected digraphs.

    Args:
        z (sympy.polys.rings.PolyElement): first generator of the ring;
            the variable marking the number of nodes
        N (int): series precision with respect to `z`

    Returns:
        sympy.polys.rings.PolyElement: Exponential GF of strongly
            connected digraphs

    """
    inverse = rs_series_inversion(gf_graphs(z,N), z, N)
    return -rs_log(
        Delta(inverse, z, sp.Rational(2,1), -sp.Rational(1,1), N),
        z, N
    )


def gf_semi_strong(z, N):
    return rs_exp(gf_strongly_connected(z, N), z, N)


#########################################
## COEFFICIENT GF OF DIGRAPH FAMILIES
#########################################

def scc_cgf(z, w, prec):
    """Coefficient GF of strongly connected digraphs.
    
    Args:
        z: first generator of the ring
        w: second generator of the ring
        prec: gf precision wrt z
        
    Returns:
        sympy.polys.rings.PolyElement
    """
    ## SSD = Exp(SCC(z)) | z -> 2^{3/2} z^2 w
    
    # this is not the most optimal way to get SSD, but we keep things simple
    # by formally manipulating the series and not optimising too much:
    SSD = rs_subs(
        rs_exp(gf_strongly_connected(z, prec), z, prec),
        {z: sp.Rational(2,1)**sp.Rational(3,2) * z**2 * w},
        z, prec
    )
    
    ## Second multiple = Delta_{alpha}^{-1/beta} ( (1 - IT(z))^2 | z -> 2zw )
    
    # after substitution, SSD is multiplied with the results of Delta operator    
    SCC = rs_mul(
        SSD,
        Delta(
            rs_subs(
                (1 - gf_irreducible_tournaments(z, prec))**2,
                {z: 2*z*w}, z, prec
            ),
            z,
            alpha, -1/beta, prec
       ),
        z, prec
    )
    
    return SCC

############################
##### COUNTING FROM GF
############################

def count_from_coefficient_gf(n, m, gf, alpha, beta):
    """Computes the coefficient of a Coefficient GF

    Args:
        n (int): index with respect to the first variable
        m (int): index with respect to the second variable
        gf (sympy.polys.rings.PolyElement): Coefficient GF
        alpha (sp.Rational): exponent base
        beta (sp.Rational): exponent power multiple, an integer
            represented by a sympy Rational

    Returns:
        sympy expression:
            `alpha^{1/beta * binomial(n,2)}[z^n w^m] gf(z, w)`

    Remark: use an algebraic field for coefficient generating functions:
        QQ.algebraic_field(alpha ** (-1 / beta))

    """
    R = gf.ring
    z = R.gens[0] # assume fixed order
    w = R.gens[1] # assume fixed order

    return R.domain.to_sympy(
#        mcoeff_at(mcoeff_at(gf, z, n), w, m)
        gf.coeff(z**n * w**m)
    ) * (alpha ** (1/beta)) ** binomial(n,2)


def count_from_exponential_gf(n, gf):
    """Computes the number of digraphs with `n` nodes
    from a family with a given Exponential GF `gf`.

    Args:
        n (int): number of nodes
        gf (sympy.polys.rings.PolyElement): Exponential GF

    Returns:
        int: n! [z^n] gf(z)
    """
    R = gf.ring
    z = R.gens[0] # assume fixed order
    w = R.gens[1] # assume fixed order

    return R.domain.to_sympy(gf.coeff(z**n) * sp.factorial(n))

#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
################################################## to edit
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

###############################################
##### GRAPHIC GF OF (MULTI-)DIGRAPH FAMILIES
###############################################

def gf_dags(z, w, N, M):
    """Graphic GF of simple DAGs,

    Args:
        z (sympy.polys.rings.PolyElement): first generator of the ring;
            the variable marking the number of nodes
        w (sympy.polys.rings.PolyElement): second generator of the ring;
            the variable marking the number of edges
        N (int): series precision with respect to `z`
        M (int): series precision with respect to `w`

    Returns:
        sympy.polys.rings.PolyElement: Graphic GF of directed acyclic (simple) digraphs

    """
    denominator = sum([
        (-z)**n
        / sp.factorial(n)
        * rs_pow(1 + w, -n * (n - 1) // 2, w, M)
        for n in range(N)
    ])
    return rs_trunc(rs_series_inversion(denominator, z, N), w, M)


def gf_mdags(z, w, N, M):
    """Multi-Graphic GF of multi-DAGs

    Args:
        z (sympy.polys.rings.PolyElement): first generator of the ring;
            the variable marking the number of nodes
        w (sympy.polys.rings.PolyElement): second generator of the ring;
            the variable marking the number of edges
        N (int): series precision with respect to `z`
        M (int): series precision with respect to `w`

    Returns:
        sympy.polys.rings.PolyElement: Multi-Graphic GF of directed acyclic multidigraphs

    """

    denominator = sum([
        (-z)**n
        / sp.factorial(n)
        * rs_exp(-w * n**2 / 2, w, M)
        for n in range(N)
    ])
    return rs_trunc(rs_series_inversion(denominator, z, N), w, M)


def gf_elem_multi(z, w, N, M):
    """Multi-Graphic GF of elementary multidigraphs.

    Args:
        z (sympy.polys.rings.PolyElement): first generator of the ring;
            the variable marking the number of nodes
        w (sympy.polys.rings.PolyElement): second generator of the ring;
            the variable marking the number of edges
        N (int): series precision with respect to `z`
        M (int): series precision with respect to `w`

    Returns:
        sympy.polys.rings.PolyElement: Multi-Graphic GF of elementary multidigraphs

    """
    denominator = rs_generalised_exp(1, z, w, N, M)
    return rs_trunc(rs_series_inversion(denominator, z, N), w, M)


def gf_elem_simple(z, w, N, M, d2=False):
    """Graphic GF of elementary simple digraphs

    Args:
        z (sympy.polys.rings.PolyElement): first generator of the ring;
            the variable marking the number of nodes
        w (sympy.polys.rings.PolyElement): second generator of the ring;
            the variable marking the number of edges
        N (int): series precision with respect to `z`
        M (int): series precision with respect to `w`
        d2 (bool): are 2-cycles allowed or not.
            Defaults to digraphs without 2-cycles.

    Returns:
        sympy.polys.rings.PolyElement: Graphic GF of elementary simple digraphs

    """
    basic_poly = rs_mul(rs_exp(-z, z, N), 1 - z*w, z, N)
    if d2:
        K = 1
    else:
        K = 2
    perturbation = rs_exp(sum([
            (z*w)**k / k
            for k in range(1, K+1)
        ]), z, N)
    basic_poly = rs_mul(basic_poly, perturbation, z, N)
    denominator = rs_fourier_simple(basic_poly, z, w, N, M)

    return rs_trunc(rs_series_inversion(denominator, z, N), w, M)


def gf_with_one_bicycle(z, w, N, M, multi=False, d2=False):
    """(Multi-)Graphic GF of (multi-)digraphs with one bicycle

    Args:
        z (sympy.polys.rings.PolyElement): first generator of the ring;
            the variable marking the number of nodes
        w (sympy.polys.rings.PolyElement): second generator of the ring;
            the variable marking the number of edges
        N (int): series precision with respect to `z`
        M (int): series precision with respect to `w`
        multi (bool): counting among multidigraphs or simple digraphs.
            Defaults to simple digraphs.
        d2 (bool): if digraph is simple, are 2-cycles allowed or not.
            Defaults to digraphs without 2-cycles.
            Ignored for multidigraphs.

    Returns:
        sympy.polys.rings.PolyElement: (Multi-)Graphic GF of (multi-)digraphs with one
            bicyclic strongly connected component

    """
    if multi:
        K = 0
        fourier = rs_fourier
        S_bicycle = (
            w**3 * z**2 * rs_pow(1 - z*w, -3, z, N) / 2
            + w**2 * z * rs_pow(1 - z*w, -2, z, N) / 2)
    elif d2:
        K = 1
        fourier = rs_fourier_simple
        S_bicycle = (
            w**4 * z**3 * (2 - z*w) * rs_pow(1 - z*w, -3, z, N) / 2
            + w**4 * z**3 * rs_pow(1 - z*w, -2, z, N) / 2)
    else:
        K = 2
        fourier = rs_fourier_simple
        S_bicycle = (
            w**5 * z**4 * (3 - 2*z*w) * rs_pow(1 - z*w, -3, z, N) / 2
            + w**6 * z**5 * rs_pow(1 - z*w, -2, z, N) / 2)

    C_K = sum([ (z*w)**k / k for k in range(1, K+1) ])

    basic_poly_num = rs_mul(
        S_bicycle,
        (1 - z*w) * rs_exp(-z + C_K, z, N),
        z, N)

    basic_poly_den = rs_mul(
        rs_exp(-z + C_K, z, N),
        1 - z*w,
        z, N)

    numerator = fourier(basic_poly_num, z, w, N, M)
    denominator = rs_pow(fourier(basic_poly_den, z, w, N, M), 2, z, N)

    result = rs_mul(numerator, rs_series_inversion(denominator, z, N), z, N)
    return rs_trunc(result, w, M)


########################################################
##### EXPONENTIAL GF OF STRONGLY CONNECTED DIGRAPHS
########################################################

def count_from_graphic_gf(n, m, gf, multi=False):
    """Computes the number of (multi-)digraphs with `n` nodes and `m` directed edges
    from a family with a given (Multi-)Graphic GF `gf`.

    Args:
        n (int): number of nodes
        m (int): number of edges
        gf (sympy.polys.rings.PolyElement): (Multi-)Graphic GF
        multi (bool): counting among multidigraphs or simple digraphs.
            Defaults to simple digraphs.

    Returns:
        int:
            if multi==True then `n! [z^n w^m] exp(w n^2 / 2) gf(z, w)`
            else `n! [z^n w^m] (1+w)^{binomial(n, 2)} gf(z, w)`

    """
    R = gf.ring
    z, w = R.gens

    if multi:
        return (
            rs_fourier(gf, z, w, n+1, m+1, inverse=True)
            .coeff(z**n * w**m)
            * sp.factorial(n))
    else:
        return (
            rs_fourier_simple(gf, z, w, n+1, m+1, inverse=True)
            .coeff(z**n * w**m)
            * sp.factorial(n))




############################
##### SPECIFIC EXAMPLES
############################


def count_DAGs(n, m, multi=False):
    """Computes the number of (multi-)DAGs with `n` nodes and `m` directed edges.

    Args:
        n (int): number of nodes
        m (int): number of edges
        multi (bool): counting among multidigraphs or simple digraphs.
            Defaults to simple digraphs.

    Returns:
        int: number of (multi-)DAGs
    """
    R, z, w = ring('z,w', QQ)
    if multi:
        gf = gf_mdags(z, w, n + 1, m + 1)
    else:
        gf = gf_dags(z, w, n + 1, m + 1)
    return count_from_graphic_gf(n, m, gf, multi=multi)


def count_elem(n, m, multi=False, d2=False):
    """Computes the number of elementary (multi-)digraphs with `n` nodes and `m`
    directed edges.

    Args:
        n (int): number of nodes
        m (int): number of edges
        multi (bool): counting among multidigraphs or simple digraphs.
            Defaults to simple digraphs.
        d2 (bool): if digraph is simple, are 2-cycles allowed or not.
            Defaults to digraphs without 2-cycles.
            Ignored for multidigraphs.

    Returns:
        int: number of elementary (multi-)digraphs
    """
    R, z, w = ring('z,w', QQ)
    if multi:
        gf = gf_elem_multi(z, w, n + 1, m + 1)
    else:
        gf = gf_elem_simple(z, w, n + 1, m + 1, d2=d2)
    return count_from_graphic_gf(n, m, gf, multi=multi)


def count_scc(n, m, **kwargs):
    """Computes the number of strongly connected (multi-)digraphs with `n` nodes
    and `m` directed edges.

    Args:
        n (int): number of nodes
        m (int): number of edges
        multi (bool): counting among multidigraphs or simple digraphs.
            Defaults to simple digraphs.
        d2 (bool): if digraph is simple, are 2-cycles allowed or not.
            Defaults to digraphs without 2-cycles.]
            Ignored for multidigraphs.

    Returns:
        sympy.Rational: (weighted) number of strongly connected (multi-)digraphs
    """
    R, z, w = ring('z,w', QQ)
    gf = gf_strongly_connected(z, w, n+1, m+1, **kwargs)
    return count_from_exponential_gf(n, m, gf)


def count_with_one_bicycle(n, m, **kwargs):
    """Computes the number of (multi-)digraphs with one bicycle.

    Args:
        n (int): number of nodes
        m (int): number of edges
        multi (bool): counting among multidigraphs or simple digraphs.
            Defaults to simple digraphs.
        d2 (bool): if digraph is simple, are 2-cycles allowed or not.
            Defaults to digraphs without 2-cycles.
            Ignored for multidigraphs.

    Returns:
        sympy.Rational: (weighted) number of (multi-)digraphs with one bicycle
    """
    R, z, w = ring('z,w', QQ)
    gf = gf_with_one_bicycle(z, w, n+1, m+1, **kwargs)
    multi = kwargs.get('multi', None)
    return count_from_graphic_gf(n, m, gf, multi=multi)
