# Formal power series class with fixed precision

import sympy as sp

from sympy.polys.ring_series import rs_mul, rs_exp, rs_log
from sympy.polys.ring_series import rs_trunc
from sympy.polys.ring_series import rs_subs
from sympy.polys.ring_series import rs_pow
from sympy.polys.ring_series import rs_series_inversion

from sympy.polys.rings import PolyElement

from sympy import binomial

def _tuple_replace(t, pos, value):
    """
    supplementary function to replace an element in the tuple is required
    because the powers of formal variables are represented by tuples
    """
    l = list(t)
    l[pos] = value
    return tuple(l)


def mcoeff_at(poly, x, idx):
    """
    Operation of coefficient extraction at `x^n`.
    If the input is of the form `F(z, w) = \\sum_n f_n(w) z^n`,
    the function returns `f_n(w)`, where `w` can contain several variables
    """
    get = poly.get
    R = poly.ring
    result = R.zero
    iv = R.gens.index(x)
    for k in poly.as_expr_dict().keys():
        if k[iv] == idx:
            r_idx = _tuple_replace(k, iv, 0)
            if r_idx not in result:
                result[r_idx] = poly[k]
            else:
                result[r_idx] += poly[k]
    return result


class Fps:
    """Class of formal power series with a fixed given precision with respect to z.
    
    Class Fields:
        prec (int): fixed precision

    Instance Fields:
        gf (sympy.polys.rings.PolyElement): ring_series generating function
        
    Supports standard arithmetic operations (+, *, -, /), exponentiation, division.
    Supports conversion to_graphic, to_exponential, Robinson's Delta operator.

    Indexing works as multivariate coefficient extraction.
    Example:
        >>> from sympy.polys import ring, QQ
        >>> R, z, w = ring('z, w', QQ)
        >>> ctx.set_prec(5)
        >>> gf = Fps(z + z*w**2 + 1)
        >>> gf[z,1]
        w**2 + 1

    Calling works as substitution wrt the first generator.
    If more arguments are used, more generators are used.
    Example:
        >>> from sympy.polys import ring, QQ
        >>> R, z, w = ring('z, w', QQ)
        >>> ctx.set_prec(5)
        >>> gf = Fps(z + z*w**2 + 1)
        >>> gf(w+z) # z -> w+z
        w**3 + w**2*z + w + z + 1
        >>> gf(w, z) # (z,w) -> (w,z)
        w + w*z**2 + 1 # ????
    """

    prec = 10  # default precision is 10, set_prec(prec) should be used

    def set_prec(prec):
        Fps.prec = prec

    def __init__(self, gf):
        if isinstance(gf, Fps):
            self.gf = gf.gf
        else:
            z = gf.ring.gens[0]
            self.gf = rs_trunc(gf, z, Fps.prec)
        
    def __mul__(self, other):
        z = self.gf.ring.gens[0]
        if isinstance(other, Fps):
            return Fps(rs_mul(self.gf, other.gf, z, Fps.prec))
        else:
            return Fps(other * self.gf)
    
    def __rmul__(self, other):
        return self.__mul__(other)
    
    def __add__(self, other):
        if isinstance(other, Fps):
            return Fps(self.gf + other.gf)
        else:
            return Fps(self.gf + other)

    def __radd__(self, other):
        return Fps(other + self.gf)
    
    def __sub__(self, other):
        if isinstance(other, Fps):
            return Fps(self.gf - other.gf)
        else:
            return Fps(self.gf - other)

    def __rsub__(self, other):
        return Fps(other - self.gf)
    
    def __pow__(self, n):
        z = self.gf.ring.gens[0]
        return Fps(rs_pow(self.gf, n, z, Fps.prec))
    
    def __truediv__(self, other):
        z = self.gf.ring.gens[0]
        if isinstance(other, Fps):
            return Fps(
                rs_mul(self.gf, rs_series_inversion(other.gf, z,
                    Fps.prec), z, Fps.prec)
            )
        else:
            return Fps(self.gf / other)

    def __rtruediv__(self, other):
        z = self.gf.ring.gens[0]
        return Fps(
            other * rs_series_inversion(self.gf, z, Fps.prec)
        )
    
    def __call__(self, *args):
        """Substitution z -> arg
        """
        z = self.gf.ring.gens[0]
        return Fps(rs_subs(
            self.gf, {var: arg.gf for var, arg in zip(self.gf.ring.gens, args)},
            z, Fps.prec))
    
    def __neg__(self):
        return Fps(-self.gf)
    
    def exp(self):
        z = self.gf.ring.gens[0]
        return Fps(rs_exp(self.gf, z, Fps.prec))
    
    def log(self):
        z = self.gf.ring.gens[0]
        return Fps(rs_log(self.gf, z, Fps.prec))

    def _Delta(self, alpha, beta):
        """Robinson's Delta operator (generalised)
        """
        z = self.gf.ring.gens[0]
        return sum([
            self[z,n] * alpha ** (-beta * binomial(n,2)) * z**n
            for n in range(Fps.prec)
        ])

    def to_graphic(self):
        z = self.gf.ring.gens[0]
        return self._Delta(sp.Rational(2,1), sp.Rational(1,1))
    
    def to_exponential(self):
        z = self.gf.ring.gens[0]
        return self._Delta(sp.Rational(2,1), sp.Rational(-1,1))

    def cgf_2_to_1(self):
        """Type conversion in case of CGF (2 -> 1).
        """
        return self._Delta(sp.Rational(2,1), sp.Rational(1,2))

    def cgf_1_to_2(self):
        """Type conversion in case of CGF (1 -> 2).
        """
        return self._Delta(sp.Rational(2,1), sp.Rational(-1,2))
    
    def as_expr(self):
        return self.gf.as_expr()
    
    def __repr__(self):
        return self.as_expr().__repr__()
    
    def __getitem__(self, coord):
        var, idx = coord
        if isinstance(var, Fps):
            return Fps(mcoeff_at(self.gf, var.gf, idx))
        else:
            return Fps(mcoeff_at(self.gf, var, idx))
    
    def __eq__(self, other):
        if isinstance(other, Fps):
            return self.gf == other.gf
        else:
            return self.gf == other

    def _count_from_cgf(self, n, m, alpha, beta):
        """Computes the coefficient of a Coefficient GF

        Args:
            n (int): first index
            m (int): second index
            alpha (sp.Rational): exponent base
            beta (sp.Rational): exponent power multiple, an integer
                represented by a sympy Rational

        Returns:
            Fps:
                `alpha^{1/beta * binomial(n,2)}[z^n w^m] gf(z, w)`
        """
        # assume fixed order
        z = self.gf.ring.gens[0]
        w = self.gf.ring.gens[1]

        return self[z, n][w, m] * (alpha ** (1/beta)) ** binomial(n,2) 

    def count_from_cgf(self, n, m, T):
        """Count from cgf type 2, type 1
        """
        return self._count_from_cgf(n, m, sp.Rational(2), sp.Rational(T))

    def count_from_egf(self, n):
        z = self.gf.ring.gens[0]
        return self[z, n] * sp.factorial(n)

        
#print(1 / Fps(1 + 4*w*z**2 + y*z, z, 5))
#print(Fps(z + 1 + z*w, z, 5)(z**2*w))
#print(Fps(z + 1 + z*w, z, 5)(z**2*w).mcoeff_at(z, 2))


###############################################
##### GFs OF GRAPH AND DIGRAPH FAMILIES
###############################################

def egf_graphs(arg):
    """Exponential GF of simple graphs

    Args:
        arg (fps.Fps): argument of the generating function

    Returns:
        fps.Fps
    """
    z = Fps(arg.gf.ring.gens[0])
    return sum([
        z**n / sp.factorial(n)
        * sp.Rational(2,1) ** (n * (n-1) // 2)
        for n in range(Fps.prec)
    ])(arg)


def egf_irreducible_tournaments(arg):
    """Exponential GF of irreducible tournaments
    """
    z = Fps(arg.gf.ring.gens[0])
    return (1 - 1/egf_graphs(z))(arg)


def egf_strongly_connected(arg):
    """Exponential GF of strongly connected digraphs.
    """
    z = Fps(arg.gf.ring.gens[0])
    return (
        -(1/egf_graphs(z)).to_exponential().log()
    )(arg)


def egf_semi_strong(arg):
    """Exponential GF of semi-strong digraphs.
    """
    z = Fps(arg.gf.ring.gens[0])
    return (
        1 / ((1/egf_graphs(z)).to_exponential())
    )(arg)


def ggf_digraphs_z_u_t(arg_z, arg_u, arg_t):
    """Graphic GF of digraphs.
    Args:
        `arg_z` (an expression): marks vertices.
        `arg_u` (an expression): marks source-like components.
        `arg_t` (an expression): marks the total number of components.
    """
    z = Fps(arg_z.gf.ring.gens[0])
    u = Fps(arg_z.gf.ring.gens[1])
    t = Fps(arg_z.gf.ring.gens[2])

    return (
        (t * (u-1) * egf_strongly_connected(z)).exp().to_graphic()
        /
        (-t * egf_strongly_connected(z)).exp().to_graphic()
    )(arg_z, arg_u, arg_t)


def egf_digraphs_full(arg_z, arg_u, arg_v, arg_y, arg_t):
    z,u,v,y,t = [Fps(gen) for gen in arg_z.gf.ring.gens[:5]]

    scc = egf_strongly_connected
    return (
        (scc(z) * t * (y-u-v+1)).exp()
        * (
            (scc(z) * t * (u-1)).exp().to_graphic()
            * (scc(z) * t * (v-1)).exp().to_graphic()
            / (-scc(z) * t).exp().to_graphic()
        ).to_exponential()
    )(arg_z, arg_u, arg_v, arg_y, arg_t)


#########################################
## CGFs
#########################################

def cgf_cg_type_1(z, w):
    """Coefficient GF of connected graphs, type 1
    """
    return (1 - egf_irreducible_tournaments(2*z*w))


def cgf_scc_type_2(z, w):
    """Coefficient GF of strongly connected digraphs, type 2
    """
    arg1 = 2 * z * w
    arg2 = sp.Rational(2) ** sp.Rational(3,2) * z**2 * w
    return (
        egf_semi_strong(arg2)
        * ((
            1 - egf_irreducible_tournaments(arg1)
        )**2).cgf_1_to_2()
    )


def cgf_digraphs_t_type_1(z,w,t):
    arg1 = 2 * z * w
    arg2 = sp.Rational(2) ** sp.Rational(3,2) * z**2 * w
    return t * (
        (
            ((1-t) * egf_strongly_connected(arg2)).exp()
            *
            ((1 - egf_irreducible_tournaments(arg1))**2).cgf_1_to_2()
        ).cgf_2_to_1()
    ) / (
        (-t * egf_strongly_connected(arg1)).exp().to_graphic()
    )**2


############################
##### COUNTING FROM GF
############################

def count_from_cgf(gf, n, m, alpha, beta):
    """Computes the coefficient of a Coefficient GF

    Args:
        gf (sympy.polys.rings.PolyElement): Coefficient GF
        n (int): first index
        m (int): second index
        alpha (sp.Rational): exponent base
        beta (sp.Rational): exponent power multiple, an integer
            represented by a sympy Rational

    Returns:
        Fps:
            `alpha^{1/beta * binomial(n,2)}[z^n w^m] gf(z, w)`
    """
    R = gf.ring

    # assume fixed order
    z = R.gens[0]
    w = R.gens[1]

    return R.domain.to_sympy(
#        mcoeff_at(mcoeff_at(gf, z, n), w, m)
        gf.coeff(z**n * w**m)
    ) * (alpha ** (1/beta)) ** binomial(n,2)


def count_from_exponential_gf(n, gf):
    """Computes the number of digraphs with `n` nodes
    from a family with a given Exponential GF `gf`.

    Args:
        n (int): number of nodes
        gf (sympy.polys.rings.PolyElement): Exponential GF

    Returns:
        int: n! [z^n] gf(z)
    """
    R = gf.ring
    z = R.gens[0] # assume fixed order
    w = R.gens[1] # assume fixed order

    return R.domain.to_sympy(gf.coeff(z**n) * sp.factorial(n))

