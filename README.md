# Supplementary material for complete asymptotic enumeration of digraph and 2-SAT families

This repository contains `Python` code and `IPython` notebooks for
constructing the numerical tables for our corresponding paper
"Asymptotics for graphically divergent series: dense digraphs and 2-SAT formulae"
by *Sergey Dovgal and Khaydar Nurligareev*
<!--available online as an arxiv preprint-->
<!-- to be completed later -->
<!-- <https://arxiv.org/abs/2009.12127> -->

## IPython Notebooks

The directory `ipynb` contains `IPython` notebooks. The notebooks are executed using `Python 3` and may require installing additional python libraries.

In addition, it is possible to view the contents of these notebooks without installing any additional software using `nbviewer` (links provided separately to each notebook below).

* [`Arithmetic-transformations.ipynb`][Arith]. Computer algebra
  verification of some parts related to the convolution rules and the
underlying arithmetic transformations.
* [`Digraphs.ipynb`][Digraphs]. Tables for digraph families
* `2-SAT.ipynb`. Tables for 2-SAT families

[Arith]: https://nbviewer.jupyter.org/urls/gitlab.com/sergey-dovgal/irreducible-directed-aux/-/raw/main/ipynb/Arithmetic-transformations.ipynb
[Digraphs]: https://nbviewer.jupyter.org/urls/gitlab.com/sergey-dovgal/irreducible-directed-aux/-/raw/main/ipynb/Digraphs.ipynb
[2-SAT]: https://nbviewer.jupyter.org/urls/gitlab.com/sergey-dovgal/irreducible-directed-aux/-/raw/main/ipynb/2-SAT.ipynb

## Utility libraries

Two utility libraries are provided in `lib` directory.

* `gf_families.py`: a module containing all the generating functions discussed in the paper: DAGs, elementary digraphs, digraphs with one bicyclic component, strongly connected digraphs. All the generating functions are provided in three options: multidigraphs, simple digraphs with or without 2-cycles.
* `rs_utils.py`: a complementary module for `sympy.ring_series` adding multivariate coefficient extraction and Fourier transform used in the paper

## Feedback

Contact information is available on my personal webpage
<https://sergey-dovgal.vercel.app/>
